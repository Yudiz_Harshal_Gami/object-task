const user = {
    name: "Your Name",
    address: {
        personal: {
            line1: "101",
            line2: "street Line",
            city: "NY",
            state: "WX",
            coordinates: {
                x: 35.12,
                y: -21.49,
            },
        },
        office: {
            city: "City",
            state: "WX",
            area: {
                landmark: "landmark",
            },
            coordinates: {
                x: 35.12,
                y: -21.49,
            },
        },
    },
    contact: {
        phone: {
            home: "xxx",
            office: "yyy",
        },
        other: {
            fax: '234'
        },
        email: {
            home: "xxx",
            office: "yyy",
        },
    },
};

// object to store final output
let oOutput = {};
let getObject = (user, name) => {
    for (let key in user) {

        // check that is parent key have childobject or not 

        // if not not than save it into oOutput object
        if (typeof user[key] != 'object') {
            oOutput[name + '_' + key] = user[key];
        } 
        else {
            getObject(user[key], name + '_' + key)
        }
    }

}

getObject(user, 'user')

console.log(oOutput)  